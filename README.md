# iojs-sw
 Swahili Localization.
 
 Tasfiri ya Tovuti Iojs kwa lugha ya swahili.

 Hii ni moja wapo ya mikakati ya kurudishia jamii kubwa ya Javascript.
 
##Github
* KimathiJS ([@kimathijs](https://github.com/kimathijs))
* Denzel Wamburu ([@denzelwamburu](https://github.com/denzelwamburu))

##Twitter
* KimathiJS ([@kimathijs](https://twitter.com/kimathijs))
* Denzel Wamburu ([@denzelwamburu](https://twitter.com/denzelwamburu))

##Jinsi Ya Kuchangia
Ili kuwezesha kutasfiri tovuti ya ([iojs.org](iojs.org)) kwa lugha ya swahili, wachangiaji wapaswa kufuata ([mtindo huu](https://github.com/nodejs/website/blob/master/TRANSLATION.md)).

Uko na uhuru wa kutoa maoni yako kupitia mitandao ya jamii ama tuma barua pepe kwa denzjoseph@gmail.com.



